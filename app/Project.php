<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'company_id', 'user_id', 'days',
    ];

    /** A project belongs to a user */
    public function users(){
        return $this->belongsToMany('App\User');
    }

    /** A project belongs to a company  */
    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function comments(){
        return $this->morphMany('App\Comment', 'commentable');
    }
}
