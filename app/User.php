<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'avatar', 'email', 'password', 'first_name', 'website', 'dob', 'middle_name', 'last_name', 'city', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /** Every user has many tasks
    public function tasks(){
        return $this->hasMany('App\Task');
    }
     */

    /** Every user has a role */
    public function role(){
        return $this->belongsTo('App\Role');
    }

    /** Every user has many companies */
    public function companies(){
        return $this->hasMany('App\Company');
    }

    /** A user belongs to many tasks */
    public function tasks(){
        return $this->belongsToMany('App\Task');
    }

    /** A user belongs to many tasks */
    public function projects(){
        return $this->belongsToMany('App\Project');
    }

    /** Every user has many comments */
    public function comments(){
        return $this->morphMany('App\Comment', 'commentable');
    }
}
