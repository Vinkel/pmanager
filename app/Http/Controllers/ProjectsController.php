<?php

namespace App\Http\Controllers;

use App\Project;
use App\Company;
use App\User;
use App\ProjectUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = null;
        if (Auth::check()){
            $projects = Project::where('user_id', Auth::user()->id)->get();

            return view('projects.index', ['projects' => $projects]);
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $company_id = null )
    {
        //
        $companies = null;
        if (!$company_id){
            $companies = Company::where('user_id', Auth::user()->id)->get();
        }
        return view('projects.create', ['company_id=>$company_id', 'companies'=>$companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company_id = null;
        if (Auth::check()){
            $project = Project::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'company_id' => $request->input('company_id'),
                'user_id' => Auth::user()->id,/** or $request->user()->id  */
                'days' => $request->input('days')
            ]);

            if($project){
                return redirect()->route('projects.show',['project' => $project->id])
                    ->with('success', 'Project Created Successfully');
            }
        }

        return back()->withInput()->with('errors','Error Creating New Project');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //

        /**
         *  Or alternatively use this code to retrieve the project from the database
         */
        //$project = project::where('id', $project->id)->first();

        $project = Project::find($project->id);
        return view('projects.show', ['project'=> $project]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
        $project = Project::find($project->id);
        return view('projects.edit', ['project'=> $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //Save data
        $projectUpdate = Project::where('id', $project->id)->update([
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        if ($projectUpdate){
            return redirect()->route('projects.show', ['project'=>$project->id])
                ->with('success', 'project Updated successfully');
        }

        //Redirect to another page
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
        $findproject = Project::find($project->id);
        if($findproject->delete()){

            //redirect
            return redirect()->route('projects.index')
                ->with('success','Project Deleted Successfully');
        }

        return back()->withInput()->with('errors','project could not be Deleted');
    }

    /** Add user to projects method */
    public function adduser(Request $request){
        //add user to project(s)
        //Take a project, add a user to it
        $project = Project::find($request->input('project_id'));

        if (Auth::user()->id == $project->user_id){
        $project = Project::find($request->input('project_id'));
        $user = User::where('email', $request->input('email'))->first(); // we use first() while retrieving single record and get() while retrieving many records

            //Check if user is already added to the Project
            $projectUser = ProjectUser::where('user_id', $user->id)
                                      ->where('project_id', $project->id)
                                      ->first();

            if ($projectUser){
                // If user already exists, exit
                return redirect()->route('projects.show',['project' => $project->id])
                    ->with('info', 'User '.$request->input('email').' is already a member of this project');
            }

          if ($user && $project){
           $project->users()->attach($user->id);

              return redirect()->route('projects.show',['project' => $project->id])
                  ->with('success', 'User '.$request->input('email').' Added to Project Successfully');
          }
        }
        return redirect()->route('projects.show',['project' => $project->id])
            ->with('errors', 'Error Adding User '.$request->input('email').' to the Project');
    }

}
