<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\UsersController;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Profiler\Profile;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        if (Auth::check()) {
            $user = User::find(Auth::user()->id)->first();
            return view('profile.show', ['user'=> $user]);
        }
        return view('auth.login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        $user = User::find(Auth::user()->id)->first();
        return view('profile.edit', ['user'=> $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        // Save data
        $userUpdate = User::find(Auth::user()->id)->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'first_name' => $request->input('first_name'),
            'website' => $request->input('website'),
            'dob' => $request->input('dob'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'city' => $request->input('city')
        ]);

        if ($userUpdate){
            return redirect()->route('profile.edit', ['user'=>Auth::user()->id])
                ->with('success', 'User '.$request->input('name').' Profile Updated successfully');
        }

        //Redirect to another page
        return back()->withInput()->with('errors', 'Error Updating User Profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function update_avatar(Request $request){
        //User upload of avatar
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('avatar'), $imageName);
        if ($request->hasFile('image')){
            
            $imageUpdate = User::find(Auth::user()->id)->update([
                'avatar' => $imageName
                ]);
        }
        if ($imageUpdate){
        return redirect()->back()->with('success', 'Profile Picture Updated Successfully!');
        }
    }
}
