<?php

namespace App\Http\Controllers;

use App\Project;
use App\Company;
use App\Task;
use App\User;
use App\TaskUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = null;
        $projects = null;
        if (Auth::check()){
            $tasks = Task::where('user_id', Auth::user()->id)->get();

            return view('tasks.index', ['tasks' => $tasks]);
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $company_id = null,$project_id = null )
    {
        //
        if (!$company_id && !$project_id){
            $companies = Company::where('user_id', Auth::user()->id)->get();
            $projects = Project::where('user_id', Auth::user()->id)->get();
            //$tasks = Task::where('user_id', Auth::user()->id)->get();
        }
        return view('tasks.create', ['company_id=>$company_id', 'companies'=>$companies], ['project_id=>$project_id', 'projects'=>$projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (Auth::check()){
            $task = Task::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'project_id' => $request->input('project_id'),
                'company_id' => $request->input('company_id'),
                'user_id' => Auth::user()->id,/** or $request->user()->id  */
                'days' => $request->input('days'),
                'hours' => $request->input('hours')
            ]);

            if($task){
                return redirect()->route('tasks.show',['task' => $task->id])
                    ->with('success', 'Task Created Successfully');
            }
        }

        return back()->withInput()->with('errors','Error Creating New Task');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $Task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //

        /**
         *  Or alternatively use this code to retrieve the Task from the database
         */
        //$task = Task::where('id', $task->id)->first();

        $task = Task::find($task->id);
        return view('tasks.show', ['task'=> $task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $Task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
        $task = Task::find($task->id);
        return view('tasks.edit', ['task'=> $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $Task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //Save data
        $taskUpdate = Task::where('id', $task->id)->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'days' => $request->input('days'),
            'hours' => $request->input('hours')
        ]);

        if ($taskUpdate){
            return redirect()->route('tasks.show', ['task'=>$task->id])
                ->with('success', 'Task Updated Successfully');
        }

        //Redirect to another page
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $Task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
        $findtask = Task::find($task->id);
        if($findtask->delete()){

            //redirect
            return redirect()->route('tasks.index')
                ->with('success','Task Deleted Successfully');
        }

        return back()->withInput()->with('errors','Task could not be Deleted');
    }

    /** Add user to Tasks method */
    public function adduser(Request $request){
        //add user to Task(s)
        //Take a Task, add a user to it
        $task = Task::find($request->input('task_id'));

        if (Auth::user()->id == $task->user_id){
            $task = Task::find($request->input('task_id'));
            $user = User::where('email', $request->input('email'))->first(); // we use first() while retrieving single record and get() while retrieving many records

            //Check if user is already added to the Task
            $taskUser = TaskUser::where('user_id', $user->id)
                ->where('task_id', $task->id)
                ->first();

            if ($taskUser){
                // If user already exists, exit
                return redirect()->route('tasks.show',['task' => $task->id])
                    ->with('info', 'User '.$request->input('email').' is already a member of this Task');
            }

            if ($user && $task){
                $task->users()->attach($user->id);

                return redirect()->route('tasks.show',['task' => $task->id])
                    ->with('success', 'User '.$request->input('email').' Added to Task Successfully');
            }
        }
        return redirect()->route('tasks.show',['task' => $task->id])
            ->with('errors', 'Error Adding User '.$request->input('email').' to the Task');
    }
}
