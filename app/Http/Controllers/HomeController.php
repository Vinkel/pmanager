<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Change Password Method
     */
    public function changePassword(Request $request){
        //
        if (!(Hash::check($request->get('cpassword'), Auth::user()->password))) {
            //The passwords matches
            return redirect()->back()->with('info', 'Your Current Password Does Not Match With The Password You Provided. Please Try Again.');
        }

        if (strcmp($request->get('cpassword'), $request->get('password')) == 0){
            // Current Password and New Password are same
            return redirect()->back()->with('warning', 'New Password Cannot Be Same As Your Current Password. Please Choose A Different Password.');
        }

        $validatedData = $request->validate([
            'cpassword' => 'required',
            'password'  => 'required|string|min:6|confirmed',
        ]);

        // Change Password
        $user = Auth::user();
        $user->password =bcrypt($request->get('password'));
        $user->save();

        return redirect()->back()->with('success', 'Password Updated Successfully!');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
