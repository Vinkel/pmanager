<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'project_id', 'user_id', 'company_id', 'days', 'hours',
    ];

    /** Every task belongs to a user */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /** Every task belongs to a user */
    public function project(){
        return $this->belongsTo('App\Project');
    }

    /** Every task belongs to a user */
    public function company(){
        return $this->belongsTo('App\Company');
    }

    /** A task belongs to many user */
    public function users(){
        return $this->belongsToMany('App\User');
    }

    /** Every Task has many comments */
    public function comments(){
        return $this->morphMany('App\Comment', 'commentable');
    }
}
