<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function (){
Route::resource('companies', 'CompaniesController');
Route::get('projects/create/{company_id?}', 'ProjectsController@create');
Route::resource('projects', 'ProjectsController');
Route::post('projects/adduser', 'ProjectsController@adduser')->name('projects.adduser');
Route::resource('roles', 'RolesController');
Route::get('tasks/create/{company_id? && project_id?}', 'TasksController@create');
Route::resource('tasks', 'TasksController');
Route::post('tasks/adduser', 'TasksController@adduser')->name('tasks.adduser');
Route::resource('profile', 'ProfileController');
Route::post('profile', 'ProfileController@update_avatar')->name('profile.update_avatar');
Route::resource('password', 'PasswordController');
Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');
Route::resource('comments', 'CommentsController');
});
