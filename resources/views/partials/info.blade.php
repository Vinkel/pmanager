@if (session()->has('info'))
    <div class="alert alert-dismissable alert-info fade in">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('info') !!}
        </strong>
    </div>
@endif