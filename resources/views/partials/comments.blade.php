{{-- @foreach($project->comments as $comment)
<div class="col-lg-4 col-md-4 col-sm-4">
    <h2>{{ $comment->body }}</h2>
    <p class="text-danger">{{ $comment->url }}</p>
    <p><a class="btn btn-primary" href="/projects/{{ $project->id }}" role="button">View Project <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a> </p>
</div>
@endforeach --}}

<!-- Container, Row, and Column used for Displaying Comments -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <!-- Fluid width widget -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span><i class="fa fa-comments-o" aria-hidden="true"></i></span> 
                        Recent Comments
                    </h3>
                </div>
                <div class="panel-body">
                    <ul class="media-list">
                        @foreach($project->comments as $comment)
                            <li class="media">
                                <div class="media-left">
                                    <img src="/avatar/{{ Auth::user()->avatar }}" class="img-circle" style="height: 50px !important;">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="users/{{ $comment->user->id }}">{{ $comment->user->first_name }} {{ $comment->user->middle_name }}
                                            - {{ $comment->user->email }}</a>
                                        <br>
                                        <small>
                                            commented on {{ $comment->created_at }}
                                        </small>
                                    </h4>
                                    <p>
                                        {{ $comment->body }}
                                    </p>
                                    <div class="text-left"> Proof:</div>
                                    <p>
                                        <a href="#">
                                            {{ $comment->url }}
                                        </a>
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <!--a href="#" class="btn btn-default btn-block">More Events »</a-->
                </div>
            </div>
            <!-- End fluid width widget -->