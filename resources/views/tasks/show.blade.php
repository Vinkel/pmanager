@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-md-9 col-lg-9 pull-left show">

    <!-- Jumbotron -->
    <div class=" panel panel-primary well well-lg">
        <h1>{{ $task->name }}</h1>
        <p class="lead">{{ $task->description }}</p>
        <!-- <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p> -->
    </div>

    <!-- Example row of columns -->
    <div class="row comment" style="margin: 3px;">

    @include('partials.commentstasks')

        <!--<li class="btn btn-default btn-sm pull-right"><a href="/projects/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Project</a></li>-->

        {{--@foreach($project->projects as $project)
        <div class="col-lg-4">
            <h2>{{ $project->name }}</h2>

            <p class="text-danger"> {{ $project->description }} </p>
            <p><a class="btn btn-primary" href="/projects/{{ $project->id  }}" role="button">View Project <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </a></p>
        </div>
        @endforeach --}}

        <form class="well form-horizontal comment" method="post" action="{{ route('comments.store') }}">
        {{ csrf_field() }}

                    <input type="hidden" name="commentable_type" value="App\Task">

                    <input type="hidden" name="commentable_id" value="{{ $task->id }}">

            <div class="text-center"><i class="fa fa-comments" aria-hidden="true"></i> COMMENTS</div>

            <div class="form-group">
                <label class="control-label" for="comment-content">Comment</label>
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-commenting" aria-hidden="true"></i></span>
                    <textarea style="resize: vertical;"
                              class="form-control autosize-target text-left"
                              name="body"
                              id="comment-content"
                              rows="3" spellcheck="false"
                              placeholder="Enter Comment"
                              required>
                            </textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="company-content">Proof of Work Done(Url/Photos)</label>
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-link" aria-hidden="true"></i></span>
                    <textarea style="resize: vertical;"
                              class="form-control autosize-target text-left"
                              name="url"
                              id="comment-content"
                              rows="2" spellcheck="false"
                              placeholder="Enter Url of Screenshots"
                              required>
                            </textarea>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="create" name="create"><strong>Comment </strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
            </div>

        </form>
        <div class="clearfix"></div>

    </div>

            </div>
        </div>
    </div>

</div>

<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
    <div class="sidebar-module sidebar-module-inset">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
        <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
    </div>
    <hr>

    <div class="sidebar-module">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
        <ol class="list-unstyled" style="padding-top: 4px; margin: 10px auto;">
            <li><a href="/tasks/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create New Task</a></li>
            <li><a href="/tasks/{{ $task->id }}/edit"><i class="fa fa-pencil-square" aria-hidden="true"></i> Edit</a></li>
            <li><a href="/tasks"><i class="fa fa-eye" aria-hidden="true"></i> View List of Tasks</a></li>

            @if($task->user_id == Auth::user()->id)
            <li>
                <a href="#"
                    onclick="
                     var result = confirm('Are you sure you want to delete this task?');
                       if(result){
                       event.preventDefault();
                       document.getElementById('delete-form').submit();
                       }
                        "><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                <form id="delete-form" action="{{ route('tasks.destroy', [$task->id]) }}"
                      method="post" style="display: none;">
                    <input type="hidden" name="_method" value="delete">
                    {{ csrf_field() }}
                </form>
            </li>
            @endif
            <li><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Member</a></li>
        </ol>
    </div>

    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href="#">March 2014</a></li>
        </ol>
    </div>
    -->

    <div class="sidebar-module">
        <h4><i class="fa fa-connectdevelop" aria-hidden="true"></i> Social Links</h4>
        <ol class="list-unstyled">
            <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                     GitHub</a></li>
            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                    Twitter</a></li>
            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                     Facebook</a></li>
        </ol>
        <hr>

        <h4><i class="fa fa-plus" aria-hidden="true"></i> Add Members</h4>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form class="well form-horizontal" method="post" action="{{ route('tasks.adduser') }}"> <!-- or just use projects/adduser instead of the route -->
                    {{ csrf_field() }}
                <div class="input-group">
                    <input name="task_id" type="hidden" class="form-control" value="{{ $task->id }}">
                    <input name="email" type="text" class="form-control" placeholder="Enter Email">
                    <span class="input-group-btn">
        <button class="btn btn-success" type="submit">Add Members <i class="fa fa-plus-square" aria-hidden="true"></i></button>
      </span>
                </div><!-- /input-group -->
                </form>
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->

        <h4><i class="fa fa-users" aria-hidden="true"></i> Team Members</h4>
        <ol class="list-unstyled" style="padding-top: 4px; margin: 10px auto;">
            @foreach($task->users as $user)
            <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> {{ $user->first_name }} {{ $user->middle_name }}</a></li>
            @endforeach
        </ol>

    </div>
</div>

@endsection




