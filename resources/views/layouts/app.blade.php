<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Pmanager') }}</title>

    <!-- Fav icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="{{ asset('images/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{-- asset('bootstrap-3.3.7-dist/css/bootstrap.min.css') --}}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ route('companies.index') }}">
                    {{ config('app.name', 'Pmanager') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
                    <li><a href="{{ route('register') }}">Register <i class="fa fa-user-plus"
                                                                      aria-hidden="true"></i></a></li>

                    @else
                        <li><a href="{{ route('companies.index') }}"><i class="fa fa-compass" aria-hidden="true"></i> My
                                Companies</a></li>
                        <li><a href="{{ route('projects.index') }}"><i class="fa fa-product-hunt"
                                                                       aria-hidden="true"></i> Projects</a></li>
                        <li><a href="{{ route('tasks.index') }}"><i class="fa fa-tasks" aria-hidden="true"></i>
                                Tasks</a></li>
                        @if(Auth::user()->role_id ==1)
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false" aria-haspopup="true">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    Admin <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('home') }}"><i class="fa fa-briefcase" aria-hidden="true"></i>
                                            Dashboard</a></li>
                                </ul>
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                <img class="img-rounded" src="/avatar/{{ Auth::user()->avatar }}"
                                     style="height: 25px; width: 25px;">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li><a href="/profile/{{ Auth::user()->id }}"><img class="img-circle"
                                                                                   src="/avatar/{{ Auth::user()->avatar }}"
                                                                                   style="height: 25px; width: 25px;">
                                        My Profile</a></li>

                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                                class="fa fa-sign-out" aria-hidden="true"></i>
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div id="container">
        <div class="container col-sm-9 col-md-9 col-lg-9 pull-left">

            @include('partials.errors')
            @include('partials.success')
            @include('partials.info')
            @include('partials.warning')

        </div>

        <div class="wrapper" id="body" style="padding-bottom: 80px; margin-bottom: 30px; height: auto; clear: both;">
            @yield('content')
        </div>
        <div class="clearfix"></div>

        <footer class="footer col-lg-12 col-md-12 col-sm-12 col-xs-12  navbar navbar-inverse navbar-fixed-bottom"
                style="margin-top: 20px !important;">
            <div class="container">
                <p class="text-center">Copyright &copy; 2017 - 2018 | <a style="color:#0a93a6; text-decoration:none;"
                                                                         href="#"> Pmanager</a> | All Rights Reserved.
                </p>
            </div>
        </footer>

        <div class="scroll-top-wrapper ">
                <span class="scroll-top-inner">
                    <i class="container"></i>
                </span>
        </div>

    </div>

</div>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{-- asset('bootstrap-3.3.7-dist/js/bootstrap.min.js') --}}"></script>

</body>
</html>
