@extends('layouts.app')

@section('content')
    <div class="wrapper">
    <div class="col-sm-9 col-md-9 col-lg-9 pull-left">

        <!-- Example row of columns -->
        <div class="row" style="margin: 10px;">

            <div class="col-sm-9 col-md-9 col-lg-9 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 panel panel-primary" style="background-color: white;">

                <h1 class="text-center">UPDATE PROJECT</h1>

                <form class="well form-horizontal" method="post" action="{{ route('companies.update', [$project->id]) }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="put">

                    <div class="form-group">
                        <label class="control-label" for="company-name">Name <span class="required">*</span> </label>
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-sitemap" aria-hidden="true"></i></span>
                            <input type="text"
                                   class="form-control"
                                   name="name"
                                   id="company-name"
                                   spellcheck="false"
                                   placeholder="Enter Company Name"
                                   required
                                   value="{{ $project->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="company-content">Description</label>
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-keyboard-o" aria-hidden="true"></i> </span>
                            <textarea style="resize: vertical;"
                                      class="form-control autosize-target text-left"
                                      name="description"
                                      id="company-content"
                                      rows="5" spellcheck="false"
                                      placeholder="Enter Company Description"
                                      required
                            >{{ $project->description }}
                                </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="update" name="update"><strong>Update Project </strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
        <div class="sidebar-module sidebar-module-inset">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
            <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
        </div>

        <div class="sidebar-module">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
            <ol class="list-unstyled">
                <li class="btn btn-default"><a href="/projects"><i class="fa fa-th-list" aria-hidden="true"></i> All Projects</a></li>
            </ol>
        </div>

        <!--
        <div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
                <li><a href="#">March 2014</a></li>
            </ol>
        </div>
        -->

        <div class="sidebar-module">
            <h4><i class="fa fa-connectdevelop" aria-hidden="true"></i> Social Links</h4>
            <ol class="list-unstyled">
                <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                        GitHub</a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                        Twitter</a></li>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                        Facebook</a></li>
            </ol>
        </div>
    </div>
    </div>
@endsection