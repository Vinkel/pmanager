@extends('layouts.app')

@section('content')
    <div class="col-sm-9 col-md-9 col-lg-9 pull-left">

        <!-- Example row of columns -->
        <div class="row">

            <div class="col-sm-9 col-md-9 col-lg-9 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 panel panel-primary" style="background-color: white;">

                <h1 class="text-center">CREATE PROJECT</h1>

                <form class="well form-horizontal" method="post" action="{{ route('projects.store') }}">
                    {{ csrf_field() }}

                    <!--input type="hidden" name="_method" value="put"-->

                    <div class="form-group">
                        <label class="control-label" for="project-name">Name <span class="required">*</span> </label>
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-list-alt" aria-hidden="true"></i></span>
                            <input type="text"
                                   class="form-control"
                                   name="name"
                                   id="project-name"
                                   spellcheck="fslse"
                                   placeholder="Enter Project Name"
                                   required>
                        </div>
                    </div>

                        @if($companies == null)
                            <input
                                    class="form-control"
                                    type="hidden"
                                    name="company_id"
                                    value="{{-- $company_id --}}"
                            />
                        @endif


                    @if($companies != null)
                    <div class="form-group">
                        <label class="control-label" for="company-content">Select Company <span class="required">*</span> </label>
                        <div class="input-group">
                             <span class="input-group-addon"> <i class="fa fa-compass" aria-hidden="true"></i></span>
                             <select class="form-control" name="company_id">
                                 @foreach($companies as $company)
                                 <option value="{{$company->id}}">{{ $company->name }}</option>
                                 @endforeach
                             </select>
                        </div>
                    </div>
                    @endif


                    <div class="form-group">
                        <label class="control-label" for="project-content">Description</label>
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-keyboard-o" aria-hidden="true"></i> </span>
                            <textarea style="resize: vertical;"
                                      class="form-control autosize-target text-left"
                                      name="description"
                                      id="project-content"
                                      rows="5" spellcheck="false"
                                      placeholder="Enter Project Description"
                                      required>
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                         <label class="control-label" for="project-name">Days <span class="required">*</span> </label>
                         <div class="input-group">
                             <span class="input-group-addon"> <i class="fa fa-calendar-plus-o" aria-hidden="true"></i></span>
                             <input type="text"
                                     class="form-control"
                                     name="days"
                                     id="days"
                                     spellcheck="false"
                                     placeholder="Enter Number of Days e.g. 10"
                                     required>
                         </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="create" name="create"><strong>Create Project </strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
        <div class="sidebar-module sidebar-module-inset">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
            <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
        </div>

        <div class="sidebar-module">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
            <ol class="list-unstyled">
                <li class="btn btn-default"><a href="/projects"><i class="fa fa-th-list" aria-hidden="true"></i> My Projects</a></li>
            </ol>
        </div>

        <!--
        <div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
                <li><a href="#">March 2014</a></li>
            </ol>
        </div>
        -->

        <div class="sidebar-module">
            <h4><i class="fa fa-connectdevelop" aria-hidden="true"></i> Social Links</h4>
            <ol class="list-unstyled">
                <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                        GitHub</a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                        Twitter</a></li>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                        Facebook</a></li>
            </ol>
        </div>
    </div>
@endsection