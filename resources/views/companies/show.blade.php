@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-md-9 col-lg-9 pull-left">

    <!-- Jumbotron -->
    <div class="jumbotron">
        <h1>{{ $company->name }}</h1>
        <p class="lead">{{ $company->description }}</p>
        <!-- <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p> -->
    </div>

    <!-- Example row of columns -->
    <div class="row" style="background-color: white; margin: 10px;">

        <li class="btn btn-default btn-sm pull-right"><a href="/projects/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Project</a></li>

        @foreach($company->projects as $project)
        <div class="col-lg-4">
            <h2>{{ $project->name }}</h2>

            <p class="text-danger"> {{ $project->description }} </p>
            <p><a class="btn btn-primary" href="/projects/{{ $project->id  }}" role="button">View Project <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </a></p>
        </div>
        @endforeach
    </div>

</div>

<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
    <div class="sidebar-module sidebar-module-inset">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
        <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
    </div>
    <hr>

    <div class="sidebar-module">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
        <ol class="list-unstyled" style="padding-top: 4px; margin: 10px auto;">
            <li><a href="/companies/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create New Company</a></li>
            <li><a href="/companies/{{ $company->id }}/edit"><i class="fa fa-pencil-square" aria-hidden="true"></i> Edit</a></li>
            <li><a href="/companies"><i class="fa fa-eye" aria-hidden="true"></i> View List of Companies</a></li>
            <li><a href="/projects/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Project</a></li>
            <li>
                <a href="#"
                    onclick="
                     var result = confirm('Are you sure you want to delete this project?');
                       if(result){
                       event.preventDefault();
                       document.getElementById('delete-form').submit();
                       }
                        "><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                <form id="delete-form" action="{{ route('companies.destroy', [$company->id]) }}"
                      method="post" style="display: none;">
                    <input type="hidden" name="_method" value="delete">
                    {{ csrf_field() }}
                </form>
            </li>
            <li><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Member</a></li>
        </ol>
    </div>

    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href="#">March 2014</a></li>
        </ol>
    </div>
    -->

    <div class="sidebar-module">
        <h4><i class="fa fa-connectdevelop" aria-hidden="true"></i> Social Links</h4>
        <ol class="list-unstyled">
            <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                     GitHub</a></li>
            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                    Twitter</a></li>
            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                     Facebook</a></li>
        </ol>
    </div>
</div>
@endsection