@extends('layouts.app')

@section('content')
    <div class="col-sm-9 col-md-9 col-lg-9 pull-left">
    <div class="panel panel-primary">

        @if(Auth::user()->id)
        <div class="panel-heading text-center"> <strong>USER {{ Auth::user()->name}}'s PROFILE</strong></div>

        <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <img src="/avatar/{{ Auth::user()->avatar }}" alt="" class="img-rounded img-responsive" />
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <h4>{{ Auth::user()->first_name}} {{ Auth::user()->middle_name }}</h4>
                            <small><cite title="{{ Auth::user()->city}}">{{ Auth::user()->city}} <i class="fa fa-map-marker" aria-hidden="true">
                                    </i></cite></small>
                            <p>
                                <i class="fa fa-envelope" aria-hidden="true"></i> {{ Auth::user()->email }}
                                <br />
                                <i class="fa fa-globe" aria-hidden="true"></i> <a href="http://www.jquery2dotnet.com">{{ Auth::user()->website }}</a>
                                <br />
                                <i class="fa fa-gift" aria-hidden="true"></i> {{ Auth::user()->dob }}</p>
                            <div class="form-group">
                                <!-- Single button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Social <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="https://plus.google.com/+jquery2dotnet/posts">Google +</a></li>
                                        <li><a href="https://www.facebook.com/jquery2dotnet">Facebook</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Gitlab</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-1 col-sm-9 col-sm-offset-1 col-md-9 col-md-offset-1 col-lg-9 col-lg-offset-1">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label class="control-label"><i class="fa fa-user-circle"></i> Username</label>
                                    <div class="input-group">
                                        <p class="text-center">{{ Auth::user()->name }}</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                     <label class="control-label"><i class="fa fa-user-circle"></i> First Name</label>
                                     <div class="input-group">
                                         <p class="text-center">{{ Auth::user()->first_name }}</p>
                                     </div>
                                </div>

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-user-circle"></i> Middle Name</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->middle_name }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-user-circle"></i> Last Name</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->last_name }}</p>
                                        </div>
                                    </div>
                            </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-envelope-o"></i> Email</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->email }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-building"></i> City</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->city }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-calendar-check-o"></i> Date of Birth</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->dob }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-globe"></i> Website</label>
                                        <div class="input-group">
                                            <p class="text-center">{{ Auth::user()->website }}</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endif
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
        <div class="sidebar-module sidebar-module-inset">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
            <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
        </div>

        <div class="sidebar-module">
            <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
            <ol class="list-unstyled">
                <li class="btn btn-default"><a href="/profile/{{ Auth::user()->id }}/edit"><i class="fa fa-pencil-square" aria-hidden="true"></i> Edit Profile</a></li>
            </ol>
        </div>

        <!--
        <div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
                <li><a href="#">March 2014</a></li>
            </ol>
        </div>
        -->

        <div class="sidebar-module">
            <h4><i class="fa fa-connectdevelop" aria-hidden="true"></i> Social Links</h4>
            <ol class="list-unstyled">
                <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                        GitHub</a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                        Twitter</a></li>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                        Facebook</a></li>
            </ol>
        </div>
    </div>
@endsection
