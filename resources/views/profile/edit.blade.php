@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-md-9 col-lg-9">

        <!-- Example row of columns -->
<div class="row">

<div class="col-sm-9 col-md-9 col-lg-9 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 panel panel-primary" style="background-color: white;">

    <h1 class="text-center">UPDATE PROFILE</h1>

<div class="well" style="padding-top: 20px;">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#home" data-toggle="tab">User Profile</a></li>
        <li><a href="#profile" data-toggle="tab">Password</a></li>
        <li><a href="#avatar" data-toggle="tab">Profile Picture</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active in" id="home">

            <form class="well form-horizontal" id="tab1" method="post" action="{{ route('profile.update', [$user->id]) }}">
                {{ csrf_field() }}

                <input type="hidden" name="_method" value="put">

                <div class="form-group">
                    <label class="control-label" for="company-name">Username <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="name"
                               id="update-profile"
                               spellcheck="false"
                               placeholder="Enter Your Username"
                               required
                               value="{{ Auth::user()->name }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">Email <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-envelope" aria-hidden="true"></i> </span>
                        <input type="email"
                               class="form-control input-xlarge"
                               name="email"
                               id="update-profile"
                               spellcheck="false"
                               placeholder="Update Email e.g. jsmith@yahoo.com"
                               required
                               value="{{ Auth::user()->email }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">First Name <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-user-circle" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="first_name"
                               id="update-profile"
                               spellcheck="false"
                               placeholder="Enter First Name"
                               required
                               value="{{ Auth::user()->first_name }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">Middle Name <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-user-circle" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="middle_name"
                               id="update-profile"
                               spellcheck="false"
                               placeholder="Enter Middle Name"
                               required
                               value="{{ Auth::user()->middle_name }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">Last Name <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-user-circle" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="last_name"
                               id="user-profile"
                               spellcheck="false"
                               placeholder="Enter Last Name"
                               required
                               value="{{ Auth::user()->last_name }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">Website <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-globe" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="website"
                               id="user-profile"
                               spellcheck="false"
                               placeholder="Enter Your Website"
                               required
                               value="{{ Auth::user()->website }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-name">Date Of Birth <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> </span>
                        <input type="text"
                               class="form-control input-xlarge"
                               name="dob"
                               id="user-profile"
                               spellcheck="false"
                               placeholder="Enter Your Date Of Birth (dd/mm/yyyy)"
                               required
                               value="{{ Auth::user()->dob }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="company-content">City</label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-keyboard-o" aria-hidden="true"></i> </span>
                        <textarea style="resize: vertical;"
                                  class="form-control autosize-target text-left input-xlarge"
                                  name="city"
                                  id="update-profile"
                                  rows="2" spellcheck="false"
                                  placeholder="Enter City e.g. Nairobi"
                                  required>{{ Auth::user()->city }}
                            </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary"><strong>Update Profile</strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
                </div>
            </form>
        </div>
        <div class="tab-pane fade" id="profile">
            <form class="well form-horizontal" id="tab2" method="post" action="{{ route('changePassword') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label" for="update-profile">Current Password <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-key" aria-hidden="true"></i> </span>
                        <input type="password" name="cpassword" id="update-profile" class="form-control input-xlarge" placeholder="Current Password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="update-profile">New Password <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-lock" aria-hidden="true"></i> </span>
                        <input type="password" name="password" id="update-profile" class="form-control input-xlarge" placeholder="Enter New Password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="update-profile">Confirm Password <span class="required">*</span> </label>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-lock" aria-hidden="true"></i> </span>
                        <input type="password" name="confirmpass" id="update-profile" class="form-control input-xlarge" placeholder="Confirm New Password">
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary"><strong>Update Password</strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
                </div>
            </form>
        </div>
        <div class="well tab-pane fade" id="avatar">

            <h2 class="text-center">{{ Auth::user()->name }}'s Profile Picture</h2>
            <img class="img-circle pull-left" src="/avatar/{{ Auth::user()->avatar }}" style="width: 150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px;">

            <form enctype="multipart/form-data" class="form-horizontal" id="tab3" method="post" action="{{ route('profile.update_avatar') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label" for="update-profile">Upload Profile Picture <span class="required">*</span> </label>
                    <div class="input-group">
                        <input type="file" name="image" id="image">
                    </div>
                </div><br>

                <div class="form-group">
                    <button class="btn btn-primary"><strong>Upload Picture</strong> <span><i class="fa fa-paper-plane" aria-hidden="true"></i></span></button>
                </div>
            </form>
        </div>
    </div>
    </div>

    </div>
    </div>
    </div>

<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
    <div class="sidebar-module sidebar-module-inset">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> About</h4>
        <p>Pmanager <em>is a project management tool</em> that enables people manage projects and tasks more conviniently and efficiently.</p>
    </div>
    <hr>

    <div class="sidebar-module">
        <h4><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actions Manager</h4>
        <ol class="list-unstyled">
            <li class="btn btn-default"><a href="/companies"><i class="fa fa-th-list" aria-hidden="true"></i> All Companies</a></li><br><br>
            <li class="btn btn-default"><a href="/projects"><i class="fa fa-th-list" aria-hidden="true"></i> All Projects</a></li><br><br>
            <li class="btn btn-default"><a href="/tasks"><i class="fa fa-th-list" aria-hidden="true"></i> All Tasks</a></li><br><br>
        </ol>
    </div>

    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href="#">March 2014</a></li>
        </ol>
    </div>
    -->

    <div class="sidebar-module">
        <h4>Social Links</h4>
        <ol class="list-unstyled">
            <li><a href="#"><i class="fa fa-github" aria-hidden="true"></i>
                    GitHub</a></li>
            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>
                    Twitter</a></li>
            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>
                    Facebook</a></li>
        </ol>
    </div>
</div>
@endsection

